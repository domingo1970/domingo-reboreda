
package negocio;
import java.util.List;
import dto.Cliente;
import model.dao.impl.ClienteDaoImpl;
import model.dao.ClienteDao;
/**
 *
 * @author portatil
 */
//para guardar y mostrar en la interfaz grafica
public class ClienteNE {
   ClienteDao clienteDao;
   public ClienteNE(){
       clienteDao=new ClienteDaoImpl();
       
   }
   public List<Cliente>list(){
       return clienteDao.list();
   }
   public String insertarClientes(Cliente cliente){
       return clienteDao.insert(cliente);
   }
   public Integer idCliente(){
       return clienteDao.idCliente();      
   }
   public Cliente get(Integer id){
       return clienteDao.get(id);
   }
   public String delete(Integer id){
       return clienteDao.delete(id);
   }
   public String update(Cliente cliente){
       return clienteDao.update(cliente);
   }
   
   
   
}

