package model.dao.impl;
import cone.sql.ConectaDB;
import dto.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.ClienteDao;

//metodos para la interfaz.
/**
 *
 * @author portatil
 */

//implementamos  todos los metodos de la interfaz ClienteDao
public class ClienteDaoImpl implements ClienteDao {
    public ConectaDB db;
    public ClienteDaoImpl(){
        db=new ConectaDB();
    }
//base de datos donde guardo los datos de los clientes, sale en la interfaz los datos de los clientes(en la tabla)
    public List<Cliente>list(){
    List<Cliente>list=null;
    String sql="Select * from Cliente";
    try{
        Connection cn=db.getConnection();
        PreparedStatement st=cn.prepareStatement(sql);
        ResultSet rs=st.executeQuery(sql);
        list=new ArrayList<>();
        while(rs.next()){
            Cliente c=new Cliente();
            c.setIdCliente(rs.getInt(1));
            c.setNombre(rs.getString(2));
            c.setApellidos(rs.getString(3));
            c.setDni(rs.getString(4));
            c.setDireccion(rs.getString(5));
            list.add(c);                       
        }
            
        }catch (SQLException e){
            System.out.println("Error: "+e.getMessage());
        }
        return list;
    }
   
    //metodo de inserccion de clientes
    public String insert(Cliente cliente){
        String result=null;
        String sql="INSERT INTO CLIENTE (idCliente, nombre,"+"apellidos, dni, direccion)"+"Values(?, ?, ?, ?, ?)";
        try{
            Connection cn=db.getConnection();
          PreparedStatement ps=cn.prepareStatement(sql);
          ps.setInt(1, cliente.getIdCliente());
          ps.setString(2, cliente.getNombre());
          ps.setString(3, cliente.getApellidos());
          ps.setString(4, cliente.getDni());
          ps.setString(5, cliente.getDireccion());
          ps.executeUpdate();
          
          ps.close();
          cn.close();
        }
          catch(SQLException e){
              result=e.getMessage();
        }
        return result;
    }
   
    // genera un id cliente automaticamente
    public Integer idCliente(){
        Integer id=0;
        
        String sql="select max(idCliente)+1 as codigo from cliente;";
        
        try{
            
            Connection cn=db.getConnection();
            PreparedStatement ps=cn.prepareStatement(sql);
            
            ResultSet rs=ps.executeQuery();
            
            if(rs.next()){
                id=rs.getInt(1);  
            
        }
        ps.close();
        
        cn.close();
       
        
    
    }catch(SQLException e){
 System.out.println("Error: "+e.getMessage());   

   
}
 return id;
    }

    /**
     *
     * @param id
     * @return
     */
    
    // metodo para hacer la busqueda
public Cliente get(Integer id){
Cliente cliente=null;
String sql="select * from cliente where idcliente=?";

try{
Connection cn=db.getConnection();
PreparedStatement ps=cn.prepareStatement(sql);

ps.setInt(1,id);

ResultSet rs=ps.executeQuery();
cliente=new Cliente();

if(rs.next()){
cliente.setIdCliente(rs.getInt(1));
cliente.setNombre(rs.getString(2));
cliente.setApellidos(rs.getString(3));
cliente.setDni(rs.getString(4));
cliente.setDireccion(rs.getString(5));
}
cn.close();
ps.close();

}catch(SQLException e){
System.out.println("Error: "+e.getMessage());

}
return cliente;
}

//metodo para borrar
public String delete(Integer id){
    String result=null;
    String sql="delete from cliente where idcliente=?";
    try{
        Connection cn=db.getConnection();
        PreparedStatement ps=cn.prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        cn.close();
        ps.close();
        
    }catch(SQLException e){
        result=e.getMessage();
        
    }
    return result;
    
}

    /**
     *
     * @param cliente
     * @return
     */

//metodo para grabar
    public String update(Cliente cliente){
    String result=null;
    String sql="UPDATE Cliente Set nombre=?, "+"apellidos=?, dni=?, direccion=?"+"where idCliente=?";
    
    try{
        
        Connection cn=db.getConnection();
        PreparedStatement ps=cn.prepareStatement(sql);
        ps.setString(1, cliente.getNombre());
        ps.setString(2, cliente.getApellidos());
        ps.setString(3, cliente.getDni());
        ps.setString(4, cliente.getDireccion());
        ps.setInt(5, cliente.getIdCliente());
        ps.executeUpdate();
        cn.close();
        ps.close();
    }catch(SQLException e){
    }
    return result;
        
        
    }
}
    
    
    
    

